# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.CfgGetter import addTool
addTool("LArRecUtils.LArADC2MeVToolDefault.LArADC2MeVToolDefault" , "LArADC2MeVToolDefault")
addTool("LArRecUtils.LArOFPeakRecoToolDefault.LArOFPeakRecoToolDefault","LArOFPeakRecoToolDefault")
addTool("LArRecUtils.LArParabolaPeakRecoToolDefault.LArParabolaPeakRecoToolDefault","LArParabolaPeakRecoToolDefault")
